import Vue from 'vue';
import Router from 'vue-router';
import AllBooks from './components/AllBooks.vue';
import HomePage from './components/HomePage.vue';
import Admin from './components/Admin.vue';
import Team from './components/Team.vue';
import BookDetail from './components/BookDetail.vue';
Vue.use(Router);
export default new Router({
    // mode: 'history',
    routes: [
        {
            path: '/',
            name: 'homepage',
            component: HomePage
        },            
        {
            path: '/allbooks',
            name: 'allbooks',
            component: AllBooks
        },
        {
            path: '/admin',
            name: 'admin',
            component: Admin
        }, 
        {
            path: '/bookdetail/:id/:rating?',
            name: 'bookdetail',
            component: BookDetail
        },
        {
            path: '/team',
            name: 'team',
            component: Team
        }
    ]
})
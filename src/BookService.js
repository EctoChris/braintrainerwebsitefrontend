import axios from 'axios';
// const url = 'http://localhost:5000';

class BookService {
    //Get Books:
    static async getBooks(){
        const res = await axios.get('/allbooks');
        try {
            const data = res.data;
            return data;
        } catch (err) {
            return err;
        }
    }

    //Update Book:
    static async updateBook(id, rating){
        try {
            const response = await axios.post(`/bookdetail/${id}/${rating}`);
            return response.data;
        } catch(err){
            return err;
        }
    }

    //Get Book Object:
    static async getBook(id){
        try {
            const response = await axios.get(`/bookdetail/${id}`);
            return response.data;
        } catch(err){
            return err;
        }
    }
}

export default BookService;
const path = require('path');

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    //When building out assets it will go in server folder:
    // outputDir: path.resolve(__dirname, '../public'),
    proxy: {
      '/home':{
        target: 'http://localhost:5000'
      },
      '/allbooks':{
        target: 'http://localhost:5000'
      },
      '/bookdetail':{
        target: 'http://localhost:5000'
      }
    }
  }
}